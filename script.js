/* Теоритичні питання:
1. В чому відмінність між setInterval та setTimeout? setInterval реалізує і повторює заданий код чи функцію безліч разів через заданий проміжок часу, а setTimeout реалізує
1 раз через певний проміжок часу
2. Чи можна стверджувати, що функції в setInterval та setTimeout будуть виконані рівно через той проміжок часу, який ви вказали? Ні, цей проміжок приблизний, існує
певна похибка
3. Як припинити виконання функції, яка була запланована для виклику з використанням setTimeout та setInterval? через функції clearTimeout та clearInterval
*/
// Практичне завдання 1:

// -Створіть HTML-файл із кнопкою та елементом div.
// -При натисканні кнопки використовуйте setTimeout, щоб змінити текстовий вміст елемента div через затримку 3 секунди. Новий текст повинен вказувати, що операція виконана успішно.

// let clickBtn = document.querySelector("#timeBtn");
// let divus = document.querySelector("#oper");

// clickBtn.addEventListener("click", function () {
//   divus.textContent = "Operation is starting...";
//   setTimeout(changefunc, 3000);
// });

// function changefunc() {
//   divus.textContent = "Operation is successfull!";
// }
// Практичне завдання 2:

// Реалізуйте таймер зворотного відліку, використовуючи setInterval. При завантаженні сторінки виведіть зворотний відлік від 10 до 1 в елементі div. Після досягнення 1 змініть текст на "Зворотній відлік завершено".

// let clickBtn = document.querySelector("#timeBtn");
// let divus = document.querySelector("#oper");

// let interv;
// window.addEventListener("load", function () {
//   let i = 10;
//   const countDown = setInterval(() => {
//     divus.textContent = `${i}`;
//     i--;
//     if (i < 0) {
//       clearInterval(countDown);
//       divus.textContent = "Зворотній відлік завершено";
//     }
//   }, 1000);
// });
